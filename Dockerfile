FROM continuumio/miniconda3:latest

ENTRYPOINT [ "/bin/bash", "-c" ]

# add https to apt
RUN apt-get update && apt-get install apt-transport-https

# install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get update && apt-get install nodejs yarn

WORKDIR /app

ADD package.json /app/package.json
ADD yarn.lock /app/yarn.lock
ADD environment.yml /app/environment.yml
ADD webpack.config.js /app/webpack.config.js

RUN conda env create -f environment.yml -n word2vec

RUN yarn

ADD . /app

RUN yarn run build

EXPOSE 80

RUN ["/bin/bash", "-c" , "source activate word2vec && pip install -e ."]

CMD ["source activate word2vec && gunicorn -b 0.0.0.0:8080 word2vec_demo_app:app --log-file=-"]
