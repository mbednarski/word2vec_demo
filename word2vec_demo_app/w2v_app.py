import os
from flask import Flask, logging

from word2vec_demo_app.blueprints import static_mod, webapp_mod


def create_app():
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app = Flask(__name__, static_url_path='', static_folder='')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
    app.config.from_pyfile(os.path.join(app.root_path, 'flask.cfg'))

    app.register_blueprint(static_mod, url_prefix='/static' )
    app.register_blueprint(webapp_mod, url_prefix='')


    print(app.url_map)

    return app
