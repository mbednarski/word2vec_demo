'use strict';

import * as d3 from "d3";
import {initInputLayer, updateInputLayer} from './input_layer'
import {fetchSample, performIterations, resetCorpus} from './corpus'
import {initW1Layer, updateW1Layer} from './w1_layer'
import {initW2Layer, updateW2Layer} from './w2_layer'
import {initZ1Layer, updateZ1Layer} from './z1_layer'
import {initZ2Layer, updateZ2Layer} from './z2_layer'
import {initSoftmaxLayer, updateSoftmaxLayer} from './softmax_layer'
import {initYLayer, updateYLayer} from './y_layer'
import {updateLoss, initLossPlot, updateLossPlot, resetLoss} from './loss'
import {endpoint} from "./definitions";

import './style.css'

d3.json(endpoint + '/instance')
    .post('', function(error, data){
        console.log(data);
        console.log(error);
    });

initInputLayer();
initW1Layer();
initZ1Layer();
initW2Layer();
initZ2Layer();
initSoftmaxLayer();
initYLayer();
initLossPlot();



let iterate = function (n) {

    performIterations(n)
        .then(updateInputLayer)
        .then(updateW1Layer)
        .then(updateW2Layer)
        .then(updateZ1Layer)
        .then(updateZ2Layer)
        .then(updateSoftmaxLayer)
        .then(updateYLayer)
        .then(updateLoss)
        .then(updateLossPlot)
};


d3.select('#btnIterate').on('click', function () {
    iterate(1)
  });


d3.select('#btn10Iterations').on('click', function () {
    iterate(10)
  });

d3.select('#btn1000Iterations').on('click', function () {
    iterate(1000)
  });

d3.select('#btn100Iterations').on('click', function () {
    iterate(100)
  });

d3.select('#btnResetModel').on('click', function() {
    d3.request('/reset').post('', function () {
        resetLoss()
        .then(updateInputLayer)
        .then(updateW1Layer)
        .then(updateW2Layer)
        .then(updateZ1Layer)
        .then(updateW2Layer)
        .then(updateZ2Layer)
        .then(updateSoftmaxLayer)
        .then(updateYLayer)
        .then(updateLoss)
        .then(updateLossPlot)
    });
});



