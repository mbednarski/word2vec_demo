'use strict';

import * as d3 from "d3";

import * as chroma from 'chroma-js'
let margin = { top: 20, right: 20, bottom: 30, left: 50 },
    width = 100 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

let colorscale = chroma.scale(['white', 'blue']).mode('lrgb').domain([0, 1]);

let svg;

export function updateInputLayer() {
    return new Promise(resolve => {
        d3.json('/word2vec/x', data => {
            let cells = svg.selectAll('rect')
                .data(data);

            cells.transition()
                .ease(d3.easeCubicInOut)
                .duration(200)
                .attr('fill', d => colorscale(d.val));

            resolve()
        });
    });
}

export function initInputLayer(){
    svg = d3.select('.heatmaps')
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    d3.json('/word2vec/x', function (data) {
        let xscale = d3.scaleBand().range([0, width]);
            let yscale = d3.scaleBand().range([height, 0])
                .domain(data.map(d=>d.i));

            let yaxis = d3.axisLeft()
                .scale(yscale)
                .tickFormat((d, i) => data[i].word);

            svg.append('g')
                .attr('class', 'input_layer yaxis')
                .call(yaxis);

            let cells = svg.selectAll('rect')
                     .data(data);

            cells.enter().append('g').append('rect')
                .attr('class', 'weight')
                .attr('class', 'cell')
                .attr('width', xscale.bandwidth())
                .attr('height', yscale.bandwidth())
                .attr('y', d => yscale(d.i))
                .attr('x', d => xscale(0))
                .attr('fill', d => colorscale(d.val));

            cells.exit().remove()

        });
}


