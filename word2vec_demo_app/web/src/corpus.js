import * as d3 from "d3";

let styleCenterWord = function (node) {
    node.classed('center', true)
};

let styleContextWord = function (node) {
    node.classed('context', true)
};

export function resetCorpus() {
    let allWords = d3.selectAll('.word');
    allWords.classed('context', false);
    allWords.classed('center', false);
}

export function fetchSample() {
    return new Promise(function (resolve) {
         d3.json('/word2vec/fetch_sample', function (d) {

            let allWords = d3.selectAll('.word');
            allWords.classed('context', false);
            allWords.classed('center', false);
            let centerSelector = '#sentence' + d['sentenceId'] + ' .word' + d['centerWordId'];
            let contextSelector = '#sentence' + d['sentenceId'] + ' .word' + d['contextWordId'];
            let centerWord = d3.select(centerSelector);
            let contextWord = d3.select(contextSelector);
            d3.select('#tdCenterWord').text(d['centerWord']);
            d3.select('#tdContextWord').text(d['contextWord']);

            styleCenterWord(centerWord);
            styleContextWord(contextWord);
            resolve();
        });
    });
}

export function performIterations(nIter) {
    return new Promise(function (resolve) {
         d3.json('/word2vec/perform_iterations/' + nIter, function (d) {

            let allWords = d3.selectAll('.word');
            allWords.classed('context', false);
            allWords.classed('center', false);
            let centerSelector = '#sentence' + d['sentenceId'] + ' .word' + d['centerWordId'];
            let contextSelector = '#sentence' + d['sentenceId'] + ' .word' + d['contextWordId'];
            let centerWord = d3.select(centerSelector);
            let contextWord = d3.select(contextSelector);
            d3.select('#tdCenterWord').text(d['centerWord']);
            d3.select('#tdContextWord').text(d['contextWord']);

            styleCenterWord(centerWord);
            styleContextWord(contextWord);
            resolve();
        });
    });
}


