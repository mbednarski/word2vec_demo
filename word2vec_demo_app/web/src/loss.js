import * as d3 from "d3";
import * as chroma from 'chroma-js'

let margin = { top: 20, right: 20, bottom: 30, left: 50 },
    width = 400 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

let xscale = d3.scaleLinear().domain([0, 10]).range([0, width])
let yscale = d3.scaleLinear().domain([0, 10]).range([height, 0])

let svg;
let loss_data = [];

let xaxis = d3.axisBottom().scale(xscale);
let yaxis = d3.axisLeft().scale(yscale);

export function resetLoss(){
    return new Promise(resolve => {
        loss_data = [];
        resolve();
    });
}

export function updateLossPlot() {
    return new Promise(resolve => {
        d3.json('/word2vec/loss', data => {
            loss_data.push(data);



            console.log(loss_data);

            xscale.domain([0, d3.max(loss_data, d => d.iteration)]);
            yscale.domain([0, 1.1 * d3.max(loss_data, d => d.loss)]);

            svg.selectAll('g.loss.yaxis')
                .call(yaxis);
            svg.selectAll('g.loss.xaxis')
                .call(xaxis);

            let path = d3.select('#lossPath')
                .data(loss_data);

            let line = d3.line()
                .x(d => xscale(d.iteration))
                .y(d => yscale(d.loss));

            path.attr('d', line(loss_data));

            resolve();
        });
    });
}

export function initLossPlot(){

    svg = d3.select('.lossplot')
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    let path = svg.append('path')
            .attr('stroke', 'green')
            .attr('fill', 'none')
            .attr('id', 'lossPath');

    svg.append('g')
        .attr('class', 'loss xaxis')
        .attr('transform', 'translate(0, ' + yscale(0) + ')')
        .call(xaxis);
    svg.append('g')
                .attr('class', 'loss yaxis')
                .call(yaxis);

    updateLossPlot();
}

export function updateLoss() {
        return new Promise(function (resolve) {
         d3.json('/word2vec/loss', function (d) {

            let allWords = d3.select('#loss').node().innerHTML = d.loss;

            resolve();
        });
    });
}