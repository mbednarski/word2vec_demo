'use strict';

import * as d3 from "d3";
import * as chroma from 'chroma-js';

let margin = { top: 100, right: 20, bottom: 30, left: 50 },
    width = 400 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

let colorscale = chroma.scale(['red', 'white', 'blue']).mode('lrgb').domain([-2, 2]);

let xscale = d3.scaleBand().range([0, width]);
let yscale = d3.scaleBand().range([height, 0]);

let svg;

export function updateW2Layer() {
     return new Promise(resolve => {
        d3.json('/word2vec/W2', data => {
            let cells = svg.selectAll('rect')
                .data(data);

            cells.transition()
                .ease(d3.easeCubicInOut)
                .duration(200)
                .attr('fill', d => colorscale(d.val));

            resolve();
        });
    });
}

export function initW2Layer() {
    svg = d3.select('.heatmaps')
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    d3.json('/word2vec/W2', function (data) {

        console.log(data);

        xscale.domain(data.map(d => d.j));
        xscale.padding(0);
        yscale.domain(data.map(d => d.i));

        d3.json('/word2vec/vocabulary', vocab => {
            let yaxis = d3.axisLeft()
                    .scale(yscale)
                    .tickFormat((d, i) => vocab[i]);

            svg.append('g')
                .attr('class', 'W2 yaxis')
                .call(yaxis)
        });


        let cells = svg.selectAll('rect')
            .data(data);

        cells.enter()
            .append('rect')
            .attr('class', 'weight')
            .attr('width', xscale.bandwidth())
            .attr('height', yscale.bandwidth())
            .attr('y', d=>yscale(d.i))
            .attr('x', d=>xscale(d.j))
            .attr('fill', d=>colorscale(d.val))
    });
}



