from flask import Blueprint
import os

static_dir = os.path.join(os.path.abspath(os.getcwd()), 'public_html', 'static')
print(static_dir)

static_mod = Blueprint('static', __name__,
                       static_url_path='',
                       static_folder=static_dir)
