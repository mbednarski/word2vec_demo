from flask import Blueprint, make_response, jsonify, render_template

webapp_mod = Blueprint('api', __name__, template_folder='templates')


@webapp_mod.route('/instance', methods=['POST', 'OPTIONS'])
def create_instance():
    return make_response(jsonify({
        'instance': 234
    }), 201)


@webapp_mod.route('/')
def index():
    return render_template('index.html')


@webapp_mod.route('/y')
def get_y():
    return make_response(jsonify({'y': 99}), 201)
