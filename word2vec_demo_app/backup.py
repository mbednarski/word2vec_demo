from flask import Flask, render_template, jsonify
# from flask_session import Session

from word2vec_skipgram import Skipgram, CorpusIterator
import logging
import numpy as np
import os

app = Flask(__name__,
            template_folder='web/templates')










gunicorn_error_logger = logging.getLogger('gunicorn.error')
app.logger.handlers.extend(gunicorn_error_logger.handlers)
app.logger.setLevel(logging.DEBUG)
models = {}
corpuses = {}

corpusObj = CorpusIterator()
corpusObj.generate_pairs()


def get_model(id):
    if id in models:
        return models[id]
    else:
        model = Skipgram(corpusObj.vocabulary_size, 5)
        models[id] = model
        return model


def get_corpus(id):
    if id in corpuses:
        return corpuses[id]
    else:
        corpus = CorpusIterator()
        corpus.generate_pairs()
        corpuses[id] = corpus
        return corpus


@app.route('/vocabulary')
def get_vocabulary():
    return jsonify(get_corpus().vocabulary)


@app.route('/x')
def get_x():
    x = get_model().get_x()
    response = []
    for i in range(x.size):
        response.append({
            'i': i,
            'val': np.asscalar(x[i]),
            'word': get_corpus().idx2word[i]
        })
    return jsonify(response)


@app.route('/z1')
def get_z1():
    z1 = get_model().get_z1()
    response = []
    for i in range(z1.size):
        response.append({
            'i': i,
            'val': np.asscalar(z1[i]),
        })
    return jsonify(response)


@app.route('/z2')
def get_z2():
    z2 = get_model().get_z2()
    response = []
    for i in range(z2.size):
        response.append({
            'i': i,
            'val': np.asscalar(z2[i]),
        })
    return jsonify(response)


@app.route('/softmax')
def get_softmax():
    softmax = get_model().get_softmax()
    response = []
    for i in range(softmax.size):
        response.append({
            'i': i,
            'val': np.asscalar(softmax[i]),
        })
    return jsonify(response)


@app.route('/y')
def get_y():
    y = get_model().get_y_vector()
    response = []
    for i in range(y.size):
        response.append({
            'i': i,
            'val': np.asscalar(y[i]),
        })
    return jsonify(response)


@app.route('/W1')
def get_w1():
    W1 = get_model().get_W1()
    response = []
    for i in range(W1.shape[0]):
        for j in range(W1.shape[1]):
            response.append({
                'i': i,
                'j': j,
                'val': np.asscalar(W1[i, j])})

    return jsonify(
        response
    )


@app.route('/W2')
def get_w2():
    W2 = get_model().get_W2()
    response = []
    for i in range(W2.shape[0]):
        for j in range(W2.shape[1]):
            response.append({
                'i': i,
                'j': j,
                'val': np.asscalar(W2[i, j])})

    return jsonify(
        response
    )


@app.route('/fetch_sample')
def fetch_sample():
    sample = get_corpus().fetch_sample()

    get_model().set_x(sample['centerWordIdx'])
    get_model().set_y(sample['contextWordIdx'])
    get_model().forward()
    get_model().backward()

    return jsonify(sample)


@app.route('/loss')
def get_loss():
    data = get_corpus().get_full_batch()
    loss = get_model().compute_loss(data[:, 0], data[:, 1])

    return jsonify({
        'iteration': get_model().iteration,
        'loss': loss
    })


@app.route('/perform_iterations/<int:n_iter>', methods=['POST'])
def perform_iterations(n_iter):
    for n in range(n_iter):
        sample = get_corpus().fetch_sample()
        get_model().set_x(sample['centerWordIdx'])
        get_model().set_y(sample['contextWordIdx'])
        get_model().forward()
        get_model().backward()

    return jsonify(sample)


@app.route('/reset', methods=['POST'])
def reset():
    get_model().reset()
    get_corpus().reset()
    return jsonify({})

@app.route('/instance', methods=['POST'])
def create_instance():
    id = os.urandom(20)
    model = get_model(id)
    courpus = get_corpus(id)
    return jsonify({
        'instance': id
    })

@app.route('/')

@app.route('/')
def index():
    return render_template('index.html', sentences=corpusObj.get_tokens())


if __name__ == '__main__':
    app.run(port=5000, debug=True)
