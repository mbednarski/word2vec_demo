bind = "127.0.0.1:9999"

backlog = 64

workers = 3

timeout = 60
loglevel = 'info'
accesslog = '/var/log/artofai/gunicorn_access.log'
errorlog = '/var/log/artofai/gunicorn_error.log'
