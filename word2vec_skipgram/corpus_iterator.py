import numpy as np
from collections import namedtuple

default_corpus = [
    'he is a king',
    'she is a queen',
    'he is a man',
    'she is a woman',
    'warsaw is poland capital',
    'berlin is germany capital',
    'paris is france capital',
]


class CorpusIterator:
    def __init__(self, window_size=2, corpus=default_corpus):
        self.corpus = corpus
        self.idx_pairs = []
        self.window_size = window_size
        self.vocabulary = []
        for sentence in corpus:
            for word in sentence.split():
                if word not in self.vocabulary:
                    self.vocabulary.append(word)

        self.word2idx = {w: idx for (idx, w) in enumerate(self.vocabulary)}
        self.idx2word = {idx: w for (idx, w) in enumerate(self.vocabulary)}

        self.vocabulary_size = len(self.word2idx)

    def get_tokens(self):
        tokens = [x.split() for x in self.corpus]
        return tokens

    def generate_pairs(self):
        self.idx_pairs = []
        self.samples = []
        for sentence_id, sentence in enumerate(self.corpus):
            words = sentence.split()
            indices = [self.word2idx[w] for w in words]
            for center_word_id in range(len(indices)):
                # center word, context
                # i is center word index
                for w in range(-self.window_size, self.window_size + 1):
                    context_word_id = center_word_id + w
                    if context_word_id < 0 or context_word_id >= len(indices) or center_word_id == context_word_id:
                        continue

                    context_idx = indices[context_word_id]
                    self.idx_pairs.append((indices[center_word_id], context_idx))
                    self.samples.append({
                        'sentenceId': sentence_id,
                        'centerWordId': center_word_id,
                        'contextWordId': context_word_id,
                        'centerWordIdx': indices[center_word_id],
                        'contextWordIdx': context_idx,
                        'centerWord': self.idx2word[indices[center_word_id]],
                        'contextWord': self.idx2word[context_idx]
                                        })

        self.idx_pairs = np.array(self.idx_pairs)

    def get_full_batch(self):
        return self.idx_pairs

    def get_random_batch(self, size):
        """
        Gets random batch.
        Returns list of tuples (centerIdx, contextIdx)
        :param size:
        :return:
        """
        sample = np.random.choice(len(self.idx_pairs), size)
        return self.idx_pairs[sample]

    def map_idxs_to_word(self, idxs):
        return [self.idx2word[i] for i in idxs]


