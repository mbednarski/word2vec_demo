import torch
from torch.autograd import Variable
import numpy as np
import torch.nn.functional as F


class Skipgram:
    def __init__(self, vocabulary_size, embedding_dims, epochs=500):
        self.vocabulary_size = vocabulary_size
        self.embedding_dims = embedding_dims
        self.epochs = epochs

        self.reset()

    def reset(self):
        self.W1 = Variable(torch.randn(self.embedding_dims, self.vocabulary_size), requires_grad=True).float()
        self.W2 = Variable(torch.randn(self.vocabulary_size, self.embedding_dims), requires_grad=True).float()
        self.x = Variable(torch.zeros(self.vocabulary_size)).float()

        self.z1 = torch.matmul(self.W1, self.x)
        self.z2 = torch.matmul(self.W2, self.z1)

        self.softmax = F.softmax(self.z2, dim=0)
        self.y_vector = Variable(torch.zeros(self.vocabulary_size)).float()

        self.iteration = 0

    def set_x(self, x):
        empty = np.zeros(shape=(self.vocabulary_size,))
        empty[x] = 1
        self.x = Variable(torch.from_numpy(empty)).float()

    def set_y(self, y):
        self.y = Variable(torch.from_numpy(np.array([y]))).long()
        self.y_vector = np.zeros(shape=(self.vocabulary_size,))
        self.y_vector[y] = 1
        self.y_vector = Variable(torch.from_numpy(self.y_vector)).float()

    def get_W1(self):
        return self.W1.data.numpy()

    def get_W2(self):
        return self.W2.data.numpy()

    def get_x(self):
        return self.x.data.numpy()

    def get_z1(self):
        return self.z1.data.numpy()

    def get_z2(self):
        return self.z2.data.numpy()

    def get_softmax(self):
        return self.softmax.data.numpy()

    def get_y_vector(self):
        return self.y_vector.data.numpy()

    def compute_loss(self, x, y):
        xx = np.zeros((x.size, x.max() + 1))
        xx[np.arange(x.size), x] = 1
        x = Variable(torch.from_numpy(xx.T)).float()
        y = Variable(torch.from_numpy(np.array(y))).long()

        z1 = torch.matmul(self.W1, x)
        z2 = torch.matmul(self.W2, z1)

        log_softmax = F.log_softmax(z2, dim=0)

        loss = F.nll_loss(log_softmax.t(), y)
        return loss.data[0]

    def forward(self):
        self.z1 = torch.matmul(self.W1, self.x)
        self.z2 = torch.matmul(self.W2, self.z1)
        self.softmax = F.softmax(self.z2, dim=0)

        log_softmax = F.log_softmax(self.z2, dim=0).view(1, -1)

        self.loss = F.nll_loss(log_softmax, self.y)

    def backward(self):
        self.loss.backward()

        self.W1.data -= 0.01 * self.W1.grad.data
        self.W2.data -= 0.01 * self.W2.grad.data

        self.W1.grad.data.zero_()
        self.W2.grad.data.zero_()

        self.iteration += 1
