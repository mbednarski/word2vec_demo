from word2vec_skipgram import CorpusIterator, default_corpus


def test_corpus_iterator():
    ci = CorpusIterator(window_size=4, corpus=default_corpus)

    ci.generate_pairs()

    batch = ci.get_full_batch()

    assert len(batch) == 84