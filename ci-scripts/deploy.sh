# copy files
rsync -rv . /var/www/artofai.io

# restart gunicorn
sudo systemctl restart artofai
