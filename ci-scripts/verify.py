import requests

r = requests.get('https://www.artofai.io/word2vec')
assert r.status_code == 200

r = requests.get('https://www.artofai.io/word2vec/static/bundle.js')
assert r.status_code == 200