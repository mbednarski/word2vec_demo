const path = require('path');


module.exports = {
  entry: './word2vec_demo_app/web/src/index.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve('./public_html/static')
  },
   module: {
     rules: [
       {
         test: /\.css$/,
         use: [
           'style-loader',
           'css-loader'
         ]
       }
     ]
   }
};