from setuptools import setup, find_packages

setup(name='word2vec_demo',
      version='0.1dev',
      description='Very naive Word2vec skipgram implementation',
      url='https://gitlab.com/mbednarski/word2vec_demo',
      author='mbednarski',
      author_email='xevaquor@hotmail.com',
      license='MIT',
      packages=find_packages(),
      zip_safe=False)